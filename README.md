<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# Nova

The docker image for the openstack nova service and holds script for automation of the nova-database cleanup.

## Parameters

- Environment variable `DELETION_TIME_RANGE`: The time range for deleting data from all shadow tables that is older than the provided date.

## Ephemeral vTPM

This image is patched to allow shelve and evacuate actions for vTPM enabled servers.
To archive this the vTPM bekomes ephemeral.
This is comparable with the baremetal behavior where the TPM content is gone after moving a disk to a new hardware.

## License

[Apache 2](LICENSE.txt)
