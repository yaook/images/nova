#!/bin/bash
set -ex

nova-manage --version

set +e
nova-manage db archive_deleted_rows --until-complete --verbose --max_rows 500 --all-cells
RETURN_CODE=$?
set -e
if [ "$RETURN_CODE" -gt "1" ]; then
    echo "nova-manage exited with return code $RETURN_CODE"
    exit $RETURN_CODE
fi

deletion_target_date=$(date --date="${DELETION_TIME_RANGE} day ago" +%Y-%m-%d)
echo "Deleting data before ${deletion_target_date}"

set +e
nova-manage db purge --before "${deletion_target_date}" --verbose --all-cells
RETURN_CODE=$?
set -e
if [ "$RETURN_CODE" -gt "1" ]; then
    # return code 3 in nova-manage db purge means "no data deleted"
    if [ "$RETURN_CODE" -ne "3" ]; then
        echo "nova-manage exited with return code $RETURN_CODE"
        exit $RETURN_CODE
    fi
fi

exit 0
