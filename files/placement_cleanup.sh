#!/bin/bash
set -exo pipefail

nova-manage --version

# this command will only work with Openstack version >= Yoga
if [ "${OS_VERSION}" != "train"] || [ "${OS_VERSION}" != "ussuri" ] || [ "${OS_VERSION}" != "victoria" ] || [ "${OS_VERSION}" != "wallaby" ]; then    set +e
    set +e
    nova-manage placement audit --delete --verbose
    RETURN_CODE=$?
    set -e

# Exit codes
#   0   No orphaned allocations were found
#   1   An unexpected error occurred
#   3	Orphaned allocations were found
#   4   All found orphaned allocations were deleted
#   127	Invalid input

    if [ "$RETURN_CODE" -eq "1" ] || [ "$RETURN_CODE" -eq "127" ]; then
        echo "nova-manage placement audit exited with return code $RETURN_CODE"
        exit $RETURN_CODE
    fi
fi

# this command supports Openstack version >= Rocky
if [ "${OS_VERSION}" != "queens" ]; then
    set +e
    nova-manage placement heal_allocations --verbose
    RETURN_CODE=$?
    set -e

    # Exit codes
    #   0   Command completed successfully and allocations were created.
    #   1   --max-count was reached and there are more instances to process.
    #   2   Unable to find a compute node record for a given instance.
    #   3   Unable to create (or update) allocations for an instance against its compute node resource provider.
    #   4   Command completed successfully but no allocations were created.
    #   5   Unable to query ports from neutron
    #   6   Unable to update ports in neutron
    #   7   Cannot roll back neutron port updates. Manual steps needed. The error message will indicate which neutron ports need to be changed to clean up binding:profile of the port:
    #   "$ openstack port unset <port_uuid> --binding-profile allocation"
    #   127 Invalid input.
    #   255 An unexpected error occurred.

    if [ "$RETURN_CODE" -gt "1" ] && [ "$RETURN_CODE" -ne "4" ]; then
        echo "nova-manage placement heal_allocations exited with return code $RETURN_CODE"
        exit $RETURN_CODE
    fi
fi
exit 0
