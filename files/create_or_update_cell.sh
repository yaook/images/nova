#!/bin/bash
set -euo pipefail

nova-manage --version

echo "Creating or updating cell ${CELL_NUMBER}"
cell_name="cell${CELL_NUMBER}"

db_connection_string="mysql+pymysql://${DB_USER}:${DB_USER_PASSWORD}@${DB_HOST}:3306/${DB_NAME}?charset=utf8&ssl_ca=/etc/ssl/certs/ca-bundle.crt"
transport_connection_string=""
if [[ "${cell_name}" != "cell0" ]]; then
  transport_connection_string="rabbit://${MQ_USER}:${MQ_USER_PASSWORD}@${MQ_HOST}:5671/"
fi

if ! nova-manage cell_v2 list_cells | grep " ${cell_name} " -q; then 
  echo "Cell ${CELL_NUMBER} does not exist, creating it"
  if [[ "${cell_name}" == "cell0" ]]; then
    nova-manage --debug cell_v2 map_cell0 --database_connection "${db_connection_string}"
  else
    nova-manage cell_v2 create_cell --name="${cell_name}" --verbose --database_connection "${db_connection_string}" --transport-url "${transport_connection_string}"
  fi
else
  echo "Cell ${CELL_NUMBER} already exists, updating it"
  cell_uuid=$(nova-manage cell_v2 list_cells | grep " ${cell_name} " | awk '{print $4}')
  echo "Cell uuid is ${cell_uuid}"
  if [[ "${cell_name}" == "cell0" ]]; then
    nova-manage cell_v2 update_cell --cell_uuid "${cell_uuid}" --database_connection "${db_connection_string}" --transport-url "none:///"
  else
    nova-manage cell_v2 update_cell --cell_uuid "${cell_uuid}" --database_connection "${db_connection_string}" --transport-url "${transport_connection_string}"
  fi
fi

exit 0
