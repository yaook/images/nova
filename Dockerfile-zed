##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.10-slim-bookworm

ARG openstack_release=zed
ARG novnc_branch=v1.3.0

COPY files/db_cleanup.sh /db_cleanup.sh
COPY files/placement_cleanup.sh /scripts/placement_cleanup.sh
COPY files/create_or_update_cell.sh /scripts/create_or_update_cell.sh
COPY files/policies_${openstack_release}.yaml /default_policy/policy.yaml
COPY patches-nova/* /tmp/

RUN set -eux ; \
    export LANG=C.UTF-8 && \
    export DEBIAN_FRONTEND=noninteractive && \
    export DEV_PACKAGES="python3-dev gcc libssl-dev build-essential" && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        $DEV_PACKAGES \
        git \
        netbase \
        curl; \
    git clone https://github.com/novnc/novnc --depth 1 --branch ${novnc_branch} /usr/share/novnc; \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/${openstack_release} \
     || git clone https://github.com/openstack/requirements.git --depth 1 --branch ${openstack_release}-eol \
     || git clone https://github.com/openstack/requirements.git --depth 1 --branch unmaintained/${openstack_release} ; \
    git clone https://github.com/openstack/nova.git --depth 1 --branch stable/${openstack_release} \
     || git clone https://github.com/openstack/nova.git --depth 1 --branch ${openstack_release}-eol \
     || git clone https://github.com/openstack/nova.git --depth 1 --branch unmaintained/${openstack_release} ; \
    echo "oslo.messaging>=$(pip3 show oslo.messaging | grep Version | awk '{print $2}')" > oslo_messaging_constraint.txt ; \
    pip3 install -c oslo_messaging_constraint.txt --upgrade oslo.messaging==14.7.0 || true ; \
    patch -p1 -d requirements < /tmp/dnspython.patch && \
    patch -p1 -d nova < /tmp/ephemeral-vtpm.patch && \
    patch -p1 -d nova < /tmp/backing-up-server-delete.patch && \
    patch -p1 -d nova < /tmp/nova-873388-sev-image-metadefs.patch && \
    patch -p1 -d nova < /tmp/nova-2007697-sev-handle-missing-img-properties.patch && \
    patch -p1 -d nova < /tmp/reset-the-mapped-field-of-nodes-at-service-deletion.patch && \
    patch -p1 -d nova < /tmp/test-force-cold-migrate.patch && \
    patch -p1 -d nova < /tmp/feat-max_rows-for-db-purge.patch && \
    patch -p1 -d nova < /tmp/feat-nova-manage-db-instance-events-cleanup.patch && \
    cd /nova ; \
    git apply /tmp/nova-918048-handle-neutron-client-conflict.patch ; \
    cd .. ; \
    pip3 install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache python-binary-memcached nova/ ; \
    echo "oslo.messaging>=$(pip3 show oslo.messaging | grep Version | awk '{print $2}')" > oslo_messaging_constraint.txt ; \
    pip3 install -c oslo_messaging_constraint.txt --upgrade oslo.messaging==14.7.0 || true ; \
    # The following two patches can only be removed after we upgrade to the next major version of yaook
    patch -d /usr/local/lib/python3.*/site-packages/oslo_messaging -p 2 -i /tmp/fix-rabbitmq-exchange-declare-fallback.patch ; \
    patch -d /usr/local/lib/python3.*/site-packages/nova -p 2 -i /tmp/fix-nova-heartbeat-logging-filter.patch ; \
    apt purge -y $DEV_PACKAGES && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/ /tmp && \
    rm -rf requirements nova oslo_messaging_constraint.txt; \
    test -f /etc/protocols ;

CMD ["nova-api"]
