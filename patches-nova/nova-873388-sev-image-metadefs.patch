From 54faea0196c96ae55a58cab4326277d48a59afb0 Mon Sep 17 00:00:00 2001
From: Alexey Stupnikov <aleksey.stupnikov@gmail.com>
Date: Fri, 10 Feb 2023 17:14:17 +0100
Subject: [PATCH] Fix logging in MemEncryption-related checks

Currently Nova produces ambigous error when volume-backed instance
is started using flavor with hw:mem_encryption extra_specs flag:
ImageMeta doesn't contain name if it represents Cinder volume.

This fix sligtly changes steps to get image_meta.name for
some MemEncryption-related checks where it could make any
difference.

Closes-bug: #2006952
Change-Id: Ia69e7cb18cd862f01ecfdbdc358c87af1ab8fbf6
---


diff --git a/nova/virt/hardware.py b/nova/virt/hardware.py
index 96a7198..c8f8bb2 100644
--- a/nova/virt/hardware.py
+++ b/nova/virt/hardware.py
@@ -1213,10 +1213,13 @@
             "image %(image_name)s which has hw_mem_encryption property "
             "explicitly set to %(image_val)s"
         )
+        # image_meta.name is not set if image object represents root
+        # Cinder volume.
+        image_name = (image_meta.name if 'name' in image_meta else None)
         data = {
             'flavor_name': flavor.name,
             'flavor_val': flavor_mem_enc_str,
-            'image_name': image_meta.name,
+            'image_name': image_name,
             'image_val': image_mem_enc,
         }
         raise exception.FlavorImageConflict(emsg % data)
@@ -1228,10 +1231,15 @@

     emsg = _(
         "Memory encryption requested by %(requesters)s but image "
-        "%(image_name)s doesn't have 'hw_firmware_type' property set to 'uefi'"
+        "%(image_name)s doesn't have 'hw_firmware_type' property set to "
+        "'uefi' or volume-backed instance was requested"
     )
+    # image_meta.name is not set if image object represents root Cinder
+    # volume, for this case FlavorImageConflict should be raised, but
+    # image_meta.name can't be extracted.
+    image_name = (image_meta.name if 'name' in image_meta else None)
     data = {'requesters': " and ".join(requesters),
-            'image_name': image_meta.name}
+            'image_name': image_name}
     raise exception.FlavorImageConflict(emsg % data)


@@ -1260,12 +1268,14 @@
     if mach_type is None:
         return

+    # image_meta.name is not set if image object represents root Cinder volume.
+    image_name = (image_meta.name if 'name' in image_meta else None)
     # Could be something like pc-q35-2.11 if a specific version of the
     # machine type is required, so do substring matching.
     if 'q35' not in mach_type:
         raise exception.InvalidMachineType(
             mtype=mach_type,
-            image_id=image_meta.id, image_name=image_meta.name,
+            image_id=image_meta.id, image_name=image_name,
             reason=_("q35 type is required for SEV to work"))


